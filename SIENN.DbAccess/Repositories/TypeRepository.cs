﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Entities;
using Type = SIENN.DbAccess.Entities.Type;

namespace SIENN.DbAccess.Repositories
{
	public class TypeRepository : GenericRepository<Entities.Type>
	{
		public TypeRepository(DbContext context) : base(context)
		{
		}
	}
}