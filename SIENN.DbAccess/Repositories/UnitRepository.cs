﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Entities;

namespace SIENN.DbAccess.Repositories
{
	public class UnitRepository : GenericRepository<Unit>
	{
		private DbSet<Unit> _unitEntities;

		public UnitRepository(DbContext context) : base(context)
		{
			_unitEntities = context.Set<Unit>();
		}
	}
}