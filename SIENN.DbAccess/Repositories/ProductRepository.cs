﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Entities;
using Type = SIENN.DbAccess.Entities.Type;

namespace SIENN.DbAccess.Repositories
{
	public class ProductRepository : GenericRepository<Product>
	{
		private readonly DbContext _context;
		private readonly DbSet<Type> _typeEntities;
		private readonly DbSet<Unit> _unitEntities;
		private readonly DbSet<Product> _productEntities;
		private readonly DbSet<ProductCategory> _productCatEntities;

		public ProductRepository(DbContext context) : base(context)
		{
			_context = context;
			_typeEntities = context.Set<Type>();
			_unitEntities = context.Set<Unit>();
			_productEntities = context.Set<Product>();
			_productCatEntities = context.Set<ProductCategory>();
		}

		public override Product Get(int id)
		{
			return _productEntities
				.Include(x => x.Unit)
				.Include(x => x.Type)
				.Include(x => x.ProductCategories)
				.FirstOrDefault(x => x.Code.Equals(id));
		}

		public override IEnumerable<Product> GetAll()
		{
			return _productEntities.Include(x => x.ProductCategories).ToList();
		}

		public override IEnumerable<Product> GetRange(int start, int count, Expression<Func<Product, bool>> predicate)
		{
			return _productEntities.Where(predicate).Skip(start).Take(count).Include(x => x.ProductCategories).ToList();
		}

		public override void Add(Product entity)
		{
			var type = _typeEntities.Find(entity.TypeCode);
			if (type != null) type.Products = new List<Product> {entity};

			var unit = _unitEntities.Find(entity.UnitCode);
			if (unit != null) unit.Products = new List<Product> {entity};

			base.Add(entity);
		}

		public override void Update(Product entity)
		{
			var productCategories = _productCatEntities
				.AsNoTracking()
				.Where(x => x.ProductCode.Equals(entity.Code))
				.ToList();
			var toRemove = productCategories.Except(entity.ProductCategories, new ProductCategoryComparer()).ToList();
			_productCatEntities.RemoveRange(toRemove);

			var toAdd = entity.ProductCategories.Except(productCategories, new ProductCategoryComparer()).ToList();
			_productCatEntities.AddRange(toAdd);

			base.Update(entity);
		}
	}
}