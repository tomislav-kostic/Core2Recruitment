﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess.Entities;

namespace SIENN.DbAccess
{
	public class SIENNContext : DbContext
	{
		public DbSet<Product> Products { get; set; }
		public DbSet<Category> Categories { get; set; }
		public DbSet<Entities.Type> Types { get; set; }
		public DbSet<Unit> Units { get; set; }


		public SIENNContext(DbContextOptions options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ProductCategory>()
				.HasKey(t => new {t.CategoryCode, t.ProductCode});

			modelBuilder.Entity<ProductCategory>()
				.HasOne(pt => pt.Product)
				.WithMany(p => p.ProductCategories)
				.HasForeignKey(pt => pt.ProductCode);

			modelBuilder.Entity<ProductCategory>()
				.HasOne(pt => pt.Category)
				.WithMany(t => t.ProductCategories)
				.HasForeignKey(pt => pt.CategoryCode);
		}
	}
}