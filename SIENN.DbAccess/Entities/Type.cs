﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SIENN.DbAccess.Entities
{
	public class Type
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Code { get; set; }

		[MaxLength(255)]
		public string Description { get; set; }
		public List<Product> Products { get; set; }
	}
}