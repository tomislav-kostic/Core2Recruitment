﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SIENN.DbAccess.Entities
{
	public class Product
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Code { get; set; }

		[MaxLength(255)]
		public string Description { get; set; }

		public double Price { get; set; }
		public bool IsAvailable { get; set; }
		public DateTime DeliveryDate { get; set; }

		public int TypeCode { get; set; }

		[Required]
		public Type Type { get; set; }

		public int UnitCode { get; set; }

		[Required]
		public Unit Unit { get; set; }

		public List<ProductCategory> ProductCategories { get; set; }
	}
}