﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SIENN.DbAccess.Entities
{
	public class ProductCategory
	{
		public int ProductCode { get; set; }
		public Product Product { get; set; }

		public int CategoryCode { get; set; }
		public Category Category { get; set; }
	}
}