﻿--1.Unavailable products which delivery is expected in current month
SELECT *
FROM dbo.Products p 
WHERE MONTH(p.DeliveryDate) = MONTH(GETDATE());

--2.Available products that are assigned to more than one category
SELECT p.Code, p.DeliveryDate, p.Description, p.Price, p.TypeCode, p.UnitCode
FROM dbo.Products p 
INNER JOIN dbo.ProductCategory pc 
	ON p.Code = pc.ProductCode
INNER JOIN dbo.Categories c
	ON pc.CategoryCode = c.Code
WHERE p.IsAvailable = 'True'
GROUP BY p.Code, p.DeliveryDate, p.Price, p.Description, p.TypeCode, p.UnitCode
HAVING COUNT(p.Code)>1;

--3.Top 3 categories with info about numbers of available, assigned products with its mean price in category 
--(top 3 should display categories which mean price is the highest)
SELECT TOP 3 pc.CategoryCode, AVG(p.Price) as 'Mean price', COUNT(p.IsAvailable) as 'Number of available products'
FROM dbo.Products p
INNER JOIN dbo.ProductCategory pc
	ON p.Code = pc.ProductCode
WHERE p.IsAvailable = 'True'
GROUP BY pc.CategoryCode
ORDER BY AVG(p.Price) DESC;