﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using SIENN.DbAccess.Entities;
using SIENN.DbAccess.Repositories;
using SIENN.Services.Adapters;
using SIENN.Services.Models;

namespace SIENN.Services.Services
{
	public class ProductService : IGenericService<ProductDto>, IProductService
	{
		private readonly IGenericRepository<Product> _repository;

		public ProductService(IGenericRepository<Product> repository)
		{
			_repository = repository;
		}


		public ProductDto Get(int id) => ProductAdapter.BuildProductDto(_repository.Get(id));

		public IEnumerable<ProductDto> GetAll() => _repository.GetAll().Select(ProductAdapter.BuildProductDto);

		public void Add(ProductDto entity) => _repository.Add(ProductAdapter.BuildProduct(entity));

		public void Edit(ProductDto entity) => _repository.Update(ProductAdapter.BuildProduct(entity));

		public void Remove(int id)
		{
			var entity = _repository.Get(id);

			if (entity != null) _repository.Remove(entity);
		}

		public ProductInfoDto GetProductInfo(int code) => ProductAdapter.BuildProductInfoDto(_repository.Get(code));

		public IEnumerable<ProductDto> GetAvailableProducts(int start, int count)
		{
			return _repository.GetRange(start, count, x => x.IsAvailable).Select(ProductAdapter.BuildProductDto);
		}

		public IEnumerable<ProductDto> GetFilteredProducts(int? category, int? type, int? unit)
		{
			return _repository.GetAll()
				.Where(x => type == null || x.TypeCode.Equals(type))
				.Where(x => unit == null || x.UnitCode.Equals(unit))
				.Where(x => category == null || x.ProductCategories.Any(y => y.CategoryCode.Equals(category) && y.ProductCode.Equals(x.Code)))
				.Select(ProductAdapter.BuildProductDto);
		}
	}
}