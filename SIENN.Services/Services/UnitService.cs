﻿using System.Collections.Generic;
using System.Linq;
using SIENN.DbAccess.Entities;
using SIENN.DbAccess.Repositories;
using SIENN.Services.Adapters;
using SIENN.Services.Models;

namespace SIENN.Services.Services
{
	public class UnitService : IGenericService<UnitDto>
	{
		private readonly IGenericRepository<Unit> _repository;

		public UnitService(IGenericRepository<Unit> repository)
		{
			_repository = repository;
		}

		public UnitDto Get(int id) => UnitAdapter.BuilUnitDto(_repository.Get(id));

		public IEnumerable<UnitDto> GetAll() => _repository.GetAll().Select(UnitAdapter.BuilUnitDto);

		public void Add(UnitDto entity) => _repository.Add(UnitAdapter.BuildUnit(entity));

		public void Edit(UnitDto entity) => _repository.Update(UnitAdapter.BuildUnit(entity));

		public void Remove(int id)
		{
			var entity = _repository.Get(id);

			if (entity != null) _repository.Remove(entity);
		}
	}
}