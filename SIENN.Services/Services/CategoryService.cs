﻿using System.Collections.Generic;
using System.Linq;
using SIENN.DbAccess.Entities;
using SIENN.DbAccess.Repositories;
using SIENN.Services.Adapters;
using SIENN.Services.Models;

namespace SIENN.Services.Services
{
	public class CategoryService : IGenericService<CategoryDto>
	{
		private readonly IGenericRepository<Category> _repository;

		public CategoryService(IGenericRepository<Category> repository)
		{
			_repository = repository;
		}

		public CategoryDto Get(int id) => CategoryAdapter.BuildCategoryDto(_repository.Get(id));

		public IEnumerable<CategoryDto> GetAll() => _repository.GetAll().Select(CategoryAdapter.BuildCategoryDto);

		public void Add(CategoryDto entity) => _repository.Add(CategoryAdapter.BuildCategory(entity));

		public void Edit(CategoryDto entity) => _repository.Update(CategoryAdapter.BuildCategory(entity));

		public void Remove(int id)
		{
			var entity = _repository.Get(id);

			if (entity != null) _repository.Remove(entity);
		}
	}
}