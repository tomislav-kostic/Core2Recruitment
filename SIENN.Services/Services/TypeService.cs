﻿using System.Collections.Generic;
using System.Linq;
using SIENN.DbAccess.Entities;
using SIENN.DbAccess.Repositories;
using SIENN.Services.Adapters;
using SIENN.Services.Models;

namespace SIENN.Services.Services
{
	public class TypeService : IGenericService<TypeDto>
	{
		private readonly IGenericRepository<Type> _repository;

		public TypeService(IGenericRepository<Type> repository)
		{
			_repository = repository;
		}

		public TypeDto Get(int id) => TypeAdapter.BuildTypeDto(_repository.Get(id));

		public IEnumerable<TypeDto> GetAll() => _repository.GetAll().Select(TypeAdapter.BuildTypeDto);

		public void Add(TypeDto entity) => _repository.Add(TypeAdapter.BuildType(entity));

		public void Edit(TypeDto entity) => _repository.Update(TypeAdapter.BuildType(entity));

		public void Remove(int id)
		{
			var entity = _repository.Get(id);

			if (entity != null) _repository.Remove(entity);
		}
	}
}