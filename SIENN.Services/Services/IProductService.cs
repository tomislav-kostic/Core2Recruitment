﻿using System.Collections.Generic;
using SIENN.DbAccess.Entities;
using SIENN.Services.Models;

namespace SIENN.Services.Services
{
	public interface IProductService
	{
		ProductInfoDto GetProductInfo(int code);
		IEnumerable<ProductDto> GetFilteredProducts(int? category, int? type, int? unit);
		IEnumerable<ProductDto> GetAvailableProducts(int start, int count);
	}
}