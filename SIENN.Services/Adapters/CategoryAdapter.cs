﻿using System.Collections.Generic;
using System.Text;
using ExpressMapper.Extensions;
using SIENN.DbAccess.Entities;
using SIENN.Services.Models;
using Type = System.Type;

namespace SIENN.Services.Adapters
{
    internal class CategoryAdapter
    {

		public static CategoryDto BuildCategoryDto(Category category) => category.Map<Category, CategoryDto>();

		public static Category BuildCategory(CategoryDto categoryDto) => categoryDto.Map<CategoryDto, Category>();
	}
}
