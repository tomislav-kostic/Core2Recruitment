﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ExpressMapper;
using SIENN.DbAccess.Entities;
using SIENN.Services.Models;
using SIENN.Services.Services;
using Type = SIENN.DbAccess.Entities.Type;

namespace SIENN.Services
{
	public class MappingRegistration
	{
		public static void Register()
		{
			Mapper.Register<Type, TypeDto>();
			Mapper.Register<TypeDto, Type>();

			Mapper.Register<UnitDto, Unit>();
			Mapper.Register<Unit, UnitDto>();

			Mapper.Register<Category, CategoryDto>();
			Mapper.Register<CategoryDto, Category>();

			Mapper.Register<ProductDto, Product>();
			Mapper.Register<Product, ProductDto>()
				.Member(dest => dest.Categories, src => src.ProductCategories.Select(x => x.CategoryCode));
		}
	}
}